FROM kmallea/steamcmd

MAINTAINER Ryan Merrit <ryan.merritt@whothisbeme.xyz>

# Run commands as the steam user
USER steam

# Install KF2
RUN mkdir /home/steam/KF2Server &&\
    cd /home/steam/steamcmd &&\
    ./steamcmd.sh \
        +login anonymous \
        +force_install_dir ../KF2Server \
        +app_update 232130 validate \
        +quit

# Make server port available to host
EXPOSE 27015 7777

# This container will be executable
WORKDIR /home/steam/KF2Server
ENTRYPOINT ["./srcds_run"]
